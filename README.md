# TadKeep

Tadeusz is my beloved bearded dragon. I thought it'd be nice if his vivarium (terrarium/enclosure) could simulate the lighting conditions of his natural habitat in Australia.

Lately, I've extended this project to control my other vivarium which is a home for my two leopard geckos: Wiktoria and Elżbieta. Their vivarium stay warm thanks to heating mat on the bottom. I've created a hysteresis crate to control it - see <https://gitlab.com/mtczekajlo/hysteresis-rs>.

## System & Features

The system contains

- Espressif ESP32
- RTC DS3231
- EEPROM AT24CS32
- SHTC3 temperature and humidity sensors (one for hot and one for cold side)
- DS18B20 temperature sensor (for heat mat feedback)
- 1.3" SH1106 OLED display
- MOSFET modules for PWM-ing the LED stripes
- Solid State Relays for switching the bulbs and heat mat
- custom PCB to connect it all - see <https://gitlab.com/mtczekajlo/tadkeep-pcb>

Features

- datetime backup - RTC module
- sunrise and sunset calculation (see <https://gitlab.com/mtczekajlo/rust-sunrise-lite>) of Australia location from 26 weeks ago according to north (where I am) and south (Australia, obviously) hemisphere winter-summer inversion
- the light intensity scaling - dawn and dusk effects for the LED stripes
- drive LEDs, Solar, UVB and heat mat
- get temperature and humidity data once per second and display it on OLED
- send data to private instance of InfluxDB to gather some data
- calculate/fetch new sunrise-sunset data once per day
- HTTP server to update vivarium's settings (and store them in EEPROM)

**Dive into the code for more!**

## Photos

### PCB

Tadeusz's PCB with soldered two SSRs for Solar and UVB, terminal screws for two SHTC3 sensors, one MOSFET for warm LEDs.

![PCB Tadeusz](docs/pcb_tadeusz.jpg)

Wiktoria&Elżbieta's PCB with soldered one SSR for heat mat, terminal screws for DS18B20 sensor, one MOSFET for warm LEDs.

![PCB Wiktoria & Elżbieta](docs/pcb_wiktoria_elzbieta.jpg)

### UI

Tadeusz's UI

![UI Tadeusz](docs/ui_tadeusz.jpg)

Wiktoria&Elżbieta's UI

![UI Wiktoria & Elżbieta](docs/ui_wiktoria_elzbieta.jpg)

### HTTP Servers

![HTTP Tadeusz](docs/http_tadeusz.png)

![HTTP Wiktoria & Elżbieta](docs/http_wiktoria_elzbieta.png)

### InfluxDB Graphs

![Influx Tadeusz](docs/influx_tadeusz.png)

![Influx Wiktora & Elżbieta](docs/influx_wiktoria_elzbieta.png)

## Requirements

This project uses `git-lfs` - install it before cloning:

```bash
sudo dnf install -y git-lfs
```

Install ESP32 Rust Xtensa toolchain (see [espup](https://github.com/esp-rs/espup) or [rust-build](https://github.com/esp-rs/rust-build)):

```bash
cargo install espup ldproxy
espup install --export-file "~/.rust-esp32"
```

or

```bash
bash -c "$(wget -qO- https://raw.githubusercontent.com/esp-rs/rust-build/main/install-rust-toolchain.sh)" '' --export-file ~/.rust-esp32
echo "alias get-esp-rust='source ~/.rust-esp32'" >> ~/.zshrc
source ~/.zshrc
```

Install cargo extensions for flashing and monitoring:

```bash
cargo install espflash espmonitor
```

To edit `sdkconfig` you need PlatformIO and its cargo wrapper - install them with:

```bash
python3 -c "$(curl -fsSL https://raw.githubusercontent.com/platformio/platformio/master/scripts/get-platformio.py)"
cargo install cargo-pio --git https://github.com/ivmarkov/cargo-pio
```

To edit `sdkconfig`:

```bash
cargo pio espidf menuconfig
```

You have to set some environment variables until they will be configurable:

```bash
echo 'export SSID="MyWifiSSID"' >> ~/.rust-esp32-extras
echo 'export PASS="MyWifiPassword"' >> ~/.rust-esp32-extras
echo 'export INFLUXDB_HOST="influxdb.local:8086"' >> ~/.rust-esp32-extras
echo 'export TOKEN_TAD="MyInfluxDBTadeuszToken"' >> ~/.rust-esp32-extras
echo 'export TOKEN_WNE="MyInfluxDBWiktoriaElżbietaToken"' >> ~/.rust-esp32-extras
```

```bash
echo 'source ~/.rust-esp32-extras' >> ~/.rust-esp32
```

## How to

Use `--features` option to build an app for specific vivarium - `tad` or `wne`.

Build, flash and monitor in one call:

```bash
cargo espflash --monitor --speed 921600  # remember to add `--features`
```

## CI/CD

Projects uses [this docker image](https://gitlab.com/mtczekajlo/tadkeep-docker) for CI/CD.

This addition to the config file might be handy for easy docker container run:

```bash
docker run --pull=always --rm -it -v "${PWD}":/work -w /work registry.gitlab.com/mtczekajlo/tadkeep-docker:latest
```

## Docs

- [Datasheet](https://espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf)
- [Technical Reference Manual](https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf)
- [Hardware Design Guidelines](https://espressif.com/sites/default/files/documentation/esp32_hardware_design_guidelines_en.pdf)
- [Silicon Errata](https://espressif.com/sites/default/files/documentation/eco_and_workarounds_for_bugs_in_esp32_en.pdf)
- [esp-idf](https://docs.espressif.com/projects/esp-idf/en/v4.4.2/esp32/api-reference/index.html)

![ESP32 Pinout](https://microcontrollerslab.com/wp-content/uploads/2019/02/ESP32-pinout-mapping.png)
