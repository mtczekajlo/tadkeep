use chrono::{DateTime, Datelike, NaiveDateTime, Timelike, Utc};
use embedded_svc::sys_time::SystemTime;
use esp_idf_svc::systime::EspSystemTime;

pub struct Rtc;

impl Rtc {
    pub fn get_datetime(&self) -> NaiveDateTime {
        let naive = self.get_utc_datetime();
        DateTime::<Utc>::from_utc(naive, Utc)
            .with_timezone(&chrono_tz::Europe::Warsaw)
            .naive_local()
    }

    pub fn get_utc_datetime(&self) -> NaiveDateTime {
        let epoch = EspSystemTime {}.now().as_secs() as i64;
        NaiveDateTime::from_timestamp(epoch, 0)
    }

    pub fn set_datetime(&mut self, dt: NaiveDateTime) {
        let date = dt.date();
        let time = dt.time();

        let mut tm = esp_idf_sys::tm {
            tm_year: date.year() - 1900,
            tm_mon: date.month() as i32 - 1,
            tm_mday: date.day() as i32,
            tm_hour: time.hour() as i32,
            tm_min: time.minute() as i32,
            tm_sec: time.second() as i32,
            ..Default::default()
        };

        let t: i32;
        unsafe {
            t = esp_idf_sys::mktime(&mut tm);
        }
        let now = esp_idf_sys::timeval {
            tv_sec: t,
            tv_usec: 0,
        };

        unsafe {
            esp_idf_sys::settimeofday(&now, std::ptr::null());
        }
    }
}
