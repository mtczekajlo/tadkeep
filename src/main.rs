#![feature(let_chains)]

use anyhow::Result;
use chrono::{NaiveDate, NaiveDateTime, NaiveTime, Timelike};
#[cfg(feature = "heat_mat")]
use ds18b20::{Ds18b20, Resolution as Ds18b20Resolution};
use eeprom24x::{Eeprom24x, SlaveAddr};
#[cfg(any(feature = "solar", feature = "uvb", feature = "heat_mat"))]
use embedded_hal::digital::v2::OutputPin;
#[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
use esp_idf_hal::ledc::{config, Channel, Resolution as EspResolution, Timer};
use esp_idf_hal::prelude::*;
use esp_idf_svc::{http::server::EspHttpServer, sntp::EspSntp, wifi::EspWifi};
use esp_time::Rtc;
#[cfg(feature = "heat_mat")]
use hysteresis::*;
#[cfg(feature = "heat_mat")]
use one_wire_bus::OneWire;
use std::{
    net::Ipv4Addr,
    sync::{Arc, Mutex},
};
#[cfg(all(feature = "metrics", feature = "tad"))]
use tadkeep::influxdb::post_tad_records;
#[cfg(all(feature = "metrics", feature = "wne"))]
use tadkeep::influxdb::post_wne_records;
#[cfg(feature = "metrics")]
use tadkeep::sunrise_sunset_api;
use tadkeep::{
    esp_time,
    network::{self, start_http_server},
};
#[cfg(any(
    feature = "warm_leds",
    feature = "cool_leds",
    feature = "solar",
    feature = "uvb",
))]
use tadkeep_libs::dawn_dusk::DawnDuskTimes;
#[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
use tadkeep_libs::gamma_corr::*;

#[cfg(any(
    feature = "warm_leds",
    feature = "cool_leds",
    feature = "solar",
    feature = "uvb",
))]
use tadkeep_libs::settings::Mode;
use tadkeep_libs::{
    data::{Data, ElementStatus, SunPairSource},
    ds323x::{DateTimeAccess, Ds323x},
    gui,
    settings::{Settings, TimeMode},
    sh1106::prelude::GraphicsMode,
    sunrise_sunset,
};

use esp_idf_sys as _;

fn main() {
    esp_idf_sys::link_patches();

    let mut data = Data::new();

    let peripherals = Peripherals::take().unwrap();
    #[cfg(any(feature = "zone_sensors", feature = "heat_mat"))]
    let mut delay = esp_idf_hal::delay::Ets;

    let mut rtc = Rtc;
    rtc.set_datetime(NaiveDateTime::new(
        NaiveDate::from_ymd(2022, 8, 1),
        NaiveTime::from_hms(0, 0, 0),
    ));

    #[cfg(feature = "uvb")]
    let uvb_gpio = peripherals.pins.gpio5;
    #[cfg(feature = "heat_mat")]
    let heat_sensor_gpio = peripherals.pins.gpio16;
    #[cfg(feature = "heat_mat")]
    let heat_mat_gpio = peripherals.pins.gpio17;
    #[cfg(feature = "solar")]
    let solar_gpio = peripherals.pins.gpio18;
    #[cfg(feature = "warm_leds")]
    let led_warm_gpio = peripherals.pins.gpio19;
    #[cfg(feature = "cool_leds")]
    let led_cool_gpio = peripherals.pins.gpio21;
    let scl0 = peripherals.pins.gpio26;
    let sda0 = peripherals.pins.gpio27;
    let scl1 = peripherals.pins.gpio32;
    let sda1 = peripherals.pins.gpio33;

    #[cfg(feature = "heat_mat")]
    let heat_sensor_gpio = heat_sensor_gpio.into_input_output_od().unwrap();
    #[cfg(feature = "solar")]
    let mut solar_gpio = solar_gpio.into_input_output().unwrap();
    #[cfg(feature = "uvb")]
    let mut uvb_gpio = uvb_gpio.into_input_output().unwrap();
    #[cfg(feature = "heat_mat")]
    let mut heat_mat_gpio = heat_mat_gpio.into_input_output().unwrap();
    #[cfg(feature = "solar")]
    let _ = solar_gpio.set_low();
    #[cfg(feature = "uvb")]
    let _ = uvb_gpio.set_low();
    #[cfg(feature = "heat_mat")]
    let _ = heat_mat_gpio.set_low();

    #[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
    let config = config::TimerConfig::default()
        .frequency(1.kHz().into())
        .resolution(EspResolution::Bits16);

    #[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
    let timer = Timer::new(peripherals.ledc.timer0, &config).unwrap();

    #[cfg(feature = "warm_leds")]
    let mut led_warm = Channel::new(peripherals.ledc.channel0, &timer, led_warm_gpio).unwrap();
    #[cfg(feature = "cool_leds")]
    let mut led_cool = Channel::new(peripherals.ledc.channel1, &timer, led_cool_gpio).unwrap();
    #[cfg(feature = "warm_leds")]
    let _ = led_warm.set_duty(0);
    #[cfg(feature = "cool_leds")]
    let _ = led_cool.set_duty(0);

    let config =
        <esp_idf_hal::i2c::config::MasterConfig as Default>::default().baudrate(100.kHz().into());

    let i2c0 = esp_idf_hal::i2c::Master::<esp_idf_hal::i2c::I2C0, _, _>::new(
        peripherals.i2c0,
        esp_idf_hal::i2c::MasterPins {
            sda: sda0,
            scl: scl0,
        },
        config,
    )
    .unwrap();

    let i2c0_shared = shared_bus::new_std!(esp_idf_hal::i2c::Master<esp_idf_hal::i2c::I2C0, esp_idf_hal::gpio::Gpio27<esp_idf_hal::gpio::Unknown>, esp_idf_hal::gpio::Gpio26<esp_idf_hal::gpio::Unknown>> = i2c0).unwrap();

    let mut gui: Option<gui::Gui> = Some(gui::Gui::new());
    let mut display: GraphicsMode<_> = sh1106::Builder::new()
        .connect_i2c(i2c0_shared.acquire_i2c())
        .into();
    let _ = display.init();
    let _ = display.set_contrast(0);

    let i2c1 = esp_idf_hal::i2c::Master::<esp_idf_hal::i2c::I2C1, _, _>::new(
        peripherals.i2c1,
        esp_idf_hal::i2c::MasterPins {
            sda: sda1,
            scl: scl1,
        },
        config,
    )
    .unwrap();

    let i2c1_shared = shared_bus::new_std!(esp_idf_hal::i2c::Master<esp_idf_hal::i2c::I2C1, esp_idf_hal::gpio::Gpio33<esp_idf_hal::gpio::Unknown>, esp_idf_hal::gpio::Gpio32<esp_idf_hal::gpio::Unknown>> = i2c1).unwrap();

    let mut external_rtc = Ds323x::new_ds3231(i2c1_shared.acquire_i2c());
    if let Ok(dt) = external_rtc.datetime() {
        println!("RTC connected. Restored datetime: {:?}", dt);
        rtc.set_datetime(dt);
        data.clock = ElementStatus::On;
    }

    #[cfg(feature = "heat_mat")]
    let mut one_wire_bus = OneWire::new(heat_sensor_gpio).unwrap();
    #[cfg(feature = "heat_mat")]
    let _ = one_wire_bus.reset(&mut delay);

    #[cfg(feature = "heat_mat")]
    let mut sensor: Option<Ds18b20> = None;
    #[cfg(feature = "heat_mat")]
    let mut search_state = None;
    #[cfg(feature = "heat_mat")]
    while sensor.is_none() {
        if let Ok(Some((device_address, state))) =
            one_wire_bus.device_search(search_state.as_ref(), false, &mut delay)
        {
            search_state = Some(state);
            if device_address.family_code() != ds18b20::FAMILY_CODE {
                println!("Other device: {:?}", device_address);
                continue;
            }

            let device = Ds18b20::new::<&str>(device_address).unwrap();
            let config = device.read_data(&mut one_wire_bus, &mut delay);
            println!("Config: {:?}", config);
            let _ = device.set_config(
                5,
                85,
                Ds18b20Resolution::Bits12,
                &mut one_wire_bus,
                &mut delay,
            );
            let config = device.read_data(&mut one_wire_bus, &mut delay);
            println!("Config: {:?}", config);
            let _ = device.save_to_eeprom(&mut one_wire_bus, &mut delay);
            sensor = Some(device);
            println!("DS18B20 device: {:?}", device_address);
            data.heat_mat.sensor = ElementStatus::On;
            break;
        }
        std::thread::sleep(std::time::Duration::from_millis(100));
    }

    let mut eeprom = Eeprom24x::new_24x32(i2c1_shared.acquire_i2c(), SlaveAddr::default());

    let settings = match || -> Result<Settings> {
        let empty_settings = Settings::default();
        let mut serialized_settings = empty_settings.to_bytes().unwrap();
        for (i, byte) in serialized_settings.iter_mut().enumerate() {
            let mut tries = 10;
            'inner: while tries > 0 {
                let eeprom_result = eeprom.read_byte(i as u32);
                println!("Read EEPROM result: {:?}", eeprom_result);
                if let Ok(eeprom_byte) = eeprom_result {
                    *byte = eeprom_byte;
                    break 'inner;
                }
                tries -= 1;
                std::thread::sleep(std::time::Duration::from_millis(50));
            }
        }
        if serialized_settings.iter().any(|byte| *byte == 0xFF) {
            Err(anyhow::anyhow!("Corrupted settings has been read!"))
        } else {
            Settings::from_bytes(&serialized_settings)
                .map_err(|_| anyhow::anyhow!("Couldn't parse read bytes into settings!"))
        }
    }() {
        Ok(o) => {
            println!("Settings read successfully");
            o
        }
        Err(e) => {
            println!("Error in reading Settings: {}", e);
            Settings::default()
        }
    };

    println!("Settings: {}", settings);

    #[cfg(feature = "zone_sensors")]
    let mut shtc3_hot_zone = shtcx::shtc3(i2c1_shared.acquire_i2c());
    #[cfg(feature = "zone_sensors")]
    let mut shtc3_cold_zone = shtcx::shtc3(i2c0_shared.acquire_i2c());
    #[cfg(feature = "zone_sensors")]
    let mut hot_zone_meas: Option<Result<shtcx::Measurement>>;
    #[cfg(feature = "zone_sensors")]
    let mut cold_zone_meas: Option<Result<shtcx::Measurement>>;

    let mut wifi_ip: Option<Result<(EspWifi, Ipv4Addr)>> = None;
    let mut sntp: Option<Result<EspSntp>> = None;
    let mut sntp_time = false;

    #[cfg(feature = "tad")]
    let place = sunrise_sunset::AUSTRALIA;

    #[cfg(feature = "wne")]
    let place = sunrise_sunset::WROCLAW;

    #[cfg(any(feature = "tad", feature = "wne"))]
    {
        data.sunpair_auto = sunrise_sunset::get_default_model(
            place.clone(),
            if place.latitude > 0.0 {
                rtc.get_datetime().date()
            } else {
                rtc.get_datetime().date() - chrono::Duration::weeks(26)
            },
        );
    }

    let mut old_date = rtc.get_datetime().date();

    println!(
        "=====\n{}\n{}\n=====",
        rtc.get_datetime().format("%d.%m.%y %H:%M:%S "),
        data
    );

    #[cfg(feature = "heat_mat")]
    let mut hysteresis = Hysteresis::new(33.0, 0.5, HysteresisDirection::Up);

    let mut server: Option<EspHttpServer> = None;

    let mut old_settings = settings.clone();
    let settings = Arc::new(Mutex::new(settings));

    loop {
        let system_time = std::time::SystemTime::now();
        let now = rtc.get_datetime();
        if old_date != now.date()
            || matches!(
                (&data.sunpair_source, &wifi_ip),
                (SunPairSource::Model, Some(Ok(_)))
            )
        {
            old_date = now.date();
            #[cfg(any(feature = "tad", feature = "wne"))]
            if let (Some(Ok(_)), Ok(fetched_sunpair)) = (
                &wifi_ip,
                sunrise_sunset_api::get_sunrise_sunset(
                    &place,
                    if place.latitude > 0.0 {
                        old_date
                    } else {
                        old_date - chrono::Duration::weeks(26)
                    },
                ),
            ) {
                data.sunpair_auto = fetched_sunpair;
                data.sunpair_source = SunPairSource::Fetch;
                println!("Fetched new SunPair from web:\n{}", data.sunpair_auto);
            } else {
                data.sunpair_auto = sunrise_sunset::get_default_model(
                    place.clone(),
                    if place.latitude > 0.0 {
                        old_date
                    } else {
                        old_date - chrono::Duration::weeks(26)
                    },
                );
                data.sunpair_source = SunPairSource::Model;
                println!("Calculated new SunPair from model:\n{}", data.sunpair_auto);
            }
        }

        if let Ok(settings) = settings.lock() {
            if old_settings != *settings {
                old_settings = (*settings).clone();
                println!("New settings to be written to EEPROM: {}", old_settings);
                let serialized_settings = old_settings.to_bytes().unwrap();
                for (i, byte) in serialized_settings.iter().enumerate() {
                    let mut tries = 10;
                    'inner: while tries > 0 {
                        let eeprom_result = eeprom.write_byte(i as u32, *byte);
                        println!("Write EEPROM byte: {} result: {:?}", byte, eeprom_result);
                        if eeprom_result.is_ok() {
                            std::thread::sleep(std::time::Duration::from_millis(10));
                            break 'inner;
                        }
                        tries -= 1;
                        std::thread::sleep(std::time::Duration::from_millis(50));
                    }
                }
            }
        }

        data.wifi = ElementStatus::from(&wifi_ip);
        data.clock = ElementStatus::from(&sntp);

        if old_settings.time_mode == TimeMode::Manual {
            data.sunpair_source = SunPairSource::Manual;
        }
        data.sunpair_manual = sunrise_sunset::SunPair {
            date: now.date(),
            sunrise: old_settings.sunpair.sunrise,
            sunset: old_settings.sunpair.sunset,
        };

        #[cfg(feature = "zone_sensors")]
        {
            match shtc3_hot_zone.measure(shtcx::PowerMode::NormalMode, &mut delay) {
                Ok(combined) => {
                    hot_zone_meas = Some(Ok(combined));
                }
                Err(e) => {
                    println!("SHT0 (HOT ZONE) Error: {:?}", e);
                    hot_zone_meas = Some(Err(anyhow::anyhow!("SHT hot zone error: {:?}", e)));
                }
            }

            match shtc3_cold_zone.measure(shtcx::PowerMode::NormalMode, &mut delay) {
                Ok(combined) => {
                    cold_zone_meas = Some(Ok(combined));
                }
                Err(e) => {
                    println!("SHT1 (COLD ZONE) Error: {:?}", e);
                    cold_zone_meas = Some(Err(anyhow::anyhow!("SHT cold zone error: {:?}", e)));
                }
            }
        }

        #[cfg(any(
            feature = "warm_leds",
            feature = "cool_leds",
            feature = "solar",
            feature = "uvb",
        ))]
        let (sunrise, sunset) = match old_settings.time_mode {
            TimeMode::Auto => (data.sunpair_auto.sunrise, data.sunpair_auto.sunset),
            TimeMode::Manual => {
                data.sunpair_manual.sunrise = old_settings.sunpair.sunrise;
                data.sunpair_manual.sunset = old_settings.sunpair.sunset;
                (old_settings.sunpair.sunrise, old_settings.sunpair.sunset)
            }
        };

        #[cfg(any(
            feature = "warm_leds",
            feature = "cool_leds",
            feature = "solar",
            feature = "uvb",
        ))]
        let light_model = DawnDuskTimes::from_dawn_dusk(
            sunrise,
            sunset,
            chrono::Duration::minutes(30),
            chrono::Duration::minutes(30),
        )
        .with_max_day_warm_brightness_scale(old_settings.max_day_warm_scale)
        .with_max_day_cool_brightness_scale(old_settings.max_day_cool_scale)
        .with_max_night_brightness_scale(old_settings.max_night_scale);

        #[cfg(feature = "warm_leds")]
        {
            data.lights.warm_leds = match old_settings.warm_led_mode {
                Mode::Auto => {
                    f32::max(
                        light_model.led_warm_day.get_brightness_scale(now.time()),
                        light_model.led_night.get_brightness_scale(now.time()), // TODO: temporary 'OR' for one LED strip in the terrarium
                    )
                }
                Mode::On => 100.0,
                Mode::Off => 0.0,
            };
        }
        #[cfg(feature = "warm_leds")]
        {
            if let Some(led_warm_duty) = scale_value(u16::MAX, data.lights.warm_leds)  && let Some(led_warm_duty_corrected) = apply_gamma_correction(led_warm_duty, 2.8f32) {
                let _ = led_warm.set_duty(led_warm_duty_corrected.into());
            } else {
                println!(
                    "Couldn't calculate led_warm_duty_corrected with: scale={}",
                    data.lights.warm_leds
                );
                println!("Disabling Warm LEDs");
                let _ = led_warm.set_duty(0);
            }
        }

        #[cfg(feature = "cool_leds")]
        {
            data.lights.cool_leds = match old_settings.cool_led_mode {
                Mode::Auto => f32::max(
                    light_model.led_cool_day.get_brightness_scale(now.time()),
                    light_model.led_night.get_brightness_scale(now.time()),
                ),
                Mode::On => 100.0,
                Mode::Off => 0.0,
            };
        }
        #[cfg(feature = "cool_leds")]
        {
            if let Some(led_cool_duty) = scale_value(u16::MAX, data.lights.cool_leds) && let Some(led_cool_duty_corrected) = apply_gamma_correction(led_cool_duty, 2.8f32) {
                let _ = led_cool.set_duty(led_cool_duty_corrected.into());
            } else {
                println!(
                    "Couldn't calculate led_cool_duty_corrected with: scale={}",
                    data.lights.cool_leds
                );
                println!("Disabling Cool LEDs");
                let _ = led_cool.set_duty(0);
            }
        }

        #[cfg(feature = "solar")]
        {
            data.lights.solar = match old_settings.solar_mode {
                Mode::Auto => light_model.solar.is_on(now.time()),
                Mode::On => true,
                Mode::Off => false,
            };
        }
        #[cfg(feature = "solar")]
        if data.lights.solar {
            let _ = solar_gpio.set_high();
        } else {
            let _ = solar_gpio.set_low();
        }

        #[cfg(feature = "uvb")]
        {
            data.lights.uvb = match old_settings.uvb_mode {
                Mode::Auto => light_model.uvb.is_on(now.time()),
                Mode::On => true,
                Mode::Off => false,
            };
        }
        #[cfg(feature = "uvb")]
        if data.lights.uvb {
            let _ = uvb_gpio.set_high();
        } else {
            let _ = uvb_gpio.set_low();
        }

        // check every 5 secs
        if (now.time().second() % 5) == 0 {
            #[cfg(feature = "zone_sensors")]
            {
                data.cold_zone.sensor = ElementStatus::from(&cold_zone_meas);
                data.hot_zone.sensor = ElementStatus::from(&hot_zone_meas);

                if let Some(Ok(meas)) = &hot_zone_meas {
                    data.hot_zone.temperature = meas.temperature.as_degrees_celsius();
                    data.hot_zone.humidity = meas.humidity.as_percent();
                }

                if let Some(Ok(meas)) = &cold_zone_meas {
                    data.cold_zone.temperature = meas.temperature.as_degrees_celsius();
                    data.cold_zone.humidity = meas.humidity.as_percent();
                }
            }

            #[cfg(feature = "heat_mat")]
            {
                data.heat_mat.state = match old_settings.heat_mode {
                    Mode::Auto => {
                        let mut state = false;
                        if let Some(sensor) = &sensor {
                            let _ = sensor.start_temp_measurement(&mut one_wire_bus, &mut delay);
                            Ds18b20Resolution::Bits12.delay_for_measurement_time(&mut delay);

                            let mut tries = 10;
                            while tries > 0 {
                                let sensor_data = sensor.read_data(&mut one_wire_bus, &mut delay);
                                println!("One-Wire result: {:?}", sensor_data);
                                if let Ok(heat_island_temperature) = sensor_data {
                                    let heat_island_temperature =
                                        heat_island_temperature.temperature;
                                    data.heat_mat.temperature = heat_island_temperature;
                                    print!(
                                        "Temperature: {}°C | Target: {}-{}°C | ",
                                        heat_island_temperature,
                                        hysteresis.target() - hysteresis.down_margin(),
                                        hysteresis.target() + hysteresis.up_margin()
                                    );
                                    state = match hysteresis.next_direction(heat_island_temperature)
                                    {
                                        HysteresisDirection::Up => {
                                            println!("Hysteresis UP | Heat Mat ON");
                                            true
                                        }
                                        HysteresisDirection::Down => {
                                            println!("Hysteresis DOWN | Heat Mat OFF");
                                            false
                                        }
                                    };
                                    break;
                                }
                                tries -= 1;
                                std::thread::sleep(std::time::Duration::from_millis(50));
                            }
                        }
                        state
                    }
                    Mode::On => true,
                    Mode::Off => false,
                };
            }
            #[cfg(feature = "heat_mat")]
            if data.heat_mat.state {
                let _ = heat_mat_gpio.set_high();
            } else {
                let _ = heat_mat_gpio.set_low();
            }
        }

        match &wifi_ip {
            None | Some(Err(_)) => {
                let local_wifi = network::get_wifi();
                match local_wifi {
                    Ok(_) => {
                        data.wifi = ElementStatus::On;
                    }
                    Err(_) => {
                        data.wifi = ElementStatus::Error;
                    }
                };
                wifi_ip = Some(local_wifi);
            }
            Some(Ok(_)) => {
                #[cfg(feature = "metrics")]
                {
                    if now.time().second() % 5 == 0 {
                        #[cfg(feature = "tad")]
                        let _ = post_tad_records(
                            data.hot_zone.temperature,
                            data.hot_zone.humidity,
                            data.cold_zone.temperature,
                            data.cold_zone.humidity,
                        );
                        #[cfg(feature = "wne")]
                        let _ = post_wne_records(data.heat_mat.temperature, data.heat_mat.state);
                    }
                }
            }
        }

        if server.is_none() {
            let result = start_http_server(80, settings.clone());
            if let Ok(s) = result {
                server = Some(s);
            }
        }

        match sntp {
            Some(Ok(ref s)) => {
                if !sntp_time && s.get_sync_status() == esp_idf_svc::sntp::SyncStatus::Completed {
                    sntp_time = true;
                    let _ = external_rtc.set_datetime(&rtc.get_utc_datetime());
                }
            }
            Some(Err(_)) | None => {
                let local_sntp = network::get_sntp();
                match local_sntp {
                    Ok(_) => {
                        data.clock = ElementStatus::On;
                    }
                    Err(_) => {
                        data.clock = ElementStatus::Error;
                    }
                };
                sntp = Some(local_sntp);
            }
        }

        println!(
            "=====\n{}\n{}\n=====",
            now.format("%d.%m.%y %H:%M:%S "),
            data
        );
        if let Some(gui) = &mut gui {
            display.clear();
            let res = gui.draw_gui(&mut display, &data, now);
            match res {
                Ok(_) => (),
                Err(_) => {
                    let _ = display.init();
                    let _ = display.set_contrast(0);
                }
            };
            let res = display.flush();
            match res {
                Ok(_) => (),
                Err(_) => {
                    let _ = display.init();
                    let _ = display.set_contrast(0);
                }
            };
        }

        let sleep_duration = match system_time.elapsed() {
            Ok(elapsed) => {
                println!("Loop took {:?}", elapsed);
                let second = std::time::Duration::from_millis(1000);
                if elapsed > second {
                    std::time::Duration::new(0, 0)
                } else {
                    second - elapsed
                }
            }
            Err(e) => {
                println!("SystemTime elapsed error: {:?}", e);
                std::time::Duration::from_millis(900)
            }
        };

        println!("Sleep duration: {:?}", sleep_duration);
        std::thread::sleep(sleep_duration);
    }
}
