use anyhow::Result;
use chrono::DateTime;
use embedded_svc::{
    http::client::{Client, Request, Response},
    io,
};
use esp_idf_svc::http::client::EspHttpClient;
use serde::{Deserialize, Serialize};
use tadkeep_libs::sunrise_sunset::*;

const SUNRISE_SUNSET_HOST: &str = "api.sunrise-sunset.org";

#[derive(Serialize, Deserialize)]
pub struct ApiResults {
    pub sunrise: String,
    pub sunset: String,
}

#[derive(Serialize, Deserialize)]
pub struct ApiResponse {
    pub results: ApiResults,
}

pub fn get_sunrise_sunset(place: &Place, date: chrono::NaiveDate) -> Result<SunPair> {
    let url = format!(
        "http://{}/json?lat={:.7}&lng={:.7}&date={}&formatted=0",
        SUNRISE_SUNSET_HOST,
        place.latitude,
        place.longitude,
        date.format("%Y-%m-%d")
    );

    let mut client = EspHttpClient::new_default()?;

    println!("Getting content from url: {}", url);

    let mut response = client.get(&url)?.submit()?;

    let mut body = [0_u8; 1024];
    let (body, _) = io::read_max(response.reader(), &mut body)?;

    let api_response: ApiResponse = serde_json::from_slice(body)?;

    let sunrise_datetime = DateTime::parse_from_rfc3339(api_response.results.sunrise.as_str())?
        .with_timezone(place.timezone);
    let sunset_datetime = DateTime::parse_from_rfc3339(api_response.results.sunset.as_str())?
        .with_timezone(place.timezone);

    let sunpair = SunPair {
        date: sunrise_datetime.naive_local().date(),
        sunrise: sunrise_datetime.naive_local().time(),
        sunset: sunset_datetime.naive_local().time(),
    };

    println!("SunPair: {}", sunpair);

    Ok(sunpair)
}
