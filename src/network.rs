use anyhow::Result;
use core::time::Duration;
use embedded_svc::{
    http::server::{registry::Registry, Request, Response},
    io::adapters::ToStd,
    wifi::{
        ClientConfiguration, ClientConnectionStatus, ClientIpStatus, ClientStatus,
        Configuration as WifiConfiguration, Status, Wifi,
    },
};
use esp_idf_svc::{
    http::server::{Configuration as HttpConfiguration, EspHttpServer},
    netif::EspNetifStack,
    nvs::EspDefaultNvs,
    sntp::{self, EspSntp},
    sysloop::EspSysLoopStack,
    wifi::EspWifi,
};
use esp_idf_sys::{
    tcpip_adapter_get_ip_info, tcpip_adapter_if_t, tcpip_adapter_if_t_TCPIP_ADAPTER_IF_STA,
    tcpip_adapter_ip_info_t,
};
use std::{
    io::Read,
    net::Ipv4Addr,
    sync::{Arc, Mutex},
    thread::sleep,
};
use tadkeep_libs::settings::{Mode, Settings, TimeMode};

pub fn get_wifi() -> Result<(EspWifi, Ipv4Addr)> {
    let netif_stack = Arc::new(EspNetifStack::new()?);
    let sysloop_stack = Arc::new(EspSysLoopStack::new()?);
    let nvs = Arc::new(EspDefaultNvs::new()?);

    let wifi = match EspWifi::new(netif_stack, sysloop_stack, nvs) {
        Ok(wifi) => Ok(wifi),
        Err(e) => {
            println!("WiFi Error | {}", e);
            sleep(Duration::from_millis(1000));
            unsafe {
                esp_idf_sys::esp_restart();
            }
            Err(e)
        }
    };

    let mut wifi = wifi?;

    let ap_infos = wifi.scan()?;

    println!("APs:\n{:?}", ap_infos);

    let ours = ap_infos.into_iter().find(|a| a.ssid == env!("SSID"));

    let channel = if let Some(ours) = ours {
        println!(
            "Found configured access point {} on channel {}",
            env!("SSID"),
            ours.channel
        );
        Some(ours.channel)
    } else {
        println!(
            "Configured access point {} not found during scanning, will go with unknown channel",
            env!("SSID")
        );
        None
    };

    wifi.set_configuration(&WifiConfiguration::Client(ClientConfiguration {
        ssid: env!("SSID").into(),
        password: env!("PASS").into(),
        channel,
        ..Default::default()
    }))?;

    while let Err(e) = wifi
        .wait_status_with_timeout(Duration::from_secs(20), |status| !status.is_transitional())
        .map_err(|e| anyhow::anyhow!("Timeout: {:?}", e))
    {
        println!("WiFi Error | {}", e);
        std::thread::sleep(Duration::from_millis(2000));
    }

    let status = wifi.get_status();
    println!("Status: {:?}", status);

    if let Status(
        ClientStatus::Started(ClientConnectionStatus::Connected(ClientIpStatus::Done(ip_settings))),
        _,
    ) = status
    {
        println!("Connected with IP: {}", ip_settings.ip);
    } else {
        println!("Status: {:?}", status);
    }

    let tcpip_if: tcpip_adapter_if_t = tcpip_adapter_if_t_TCPIP_ADAPTER_IF_STA;
    let mut ip_info: tcpip_adapter_ip_info_t = Default::default();
    unsafe {
        tcpip_adapter_get_ip_info(tcpip_if, &mut ip_info);
    }
    let ip = Ipv4Addr::from(ip_info.ip.addr.to_be());

    Ok((wifi, ip))
}

pub fn get_sntp() -> Result<EspSntp> {
    sntp::EspSntp::new_default().map_err(|e| anyhow::anyhow!("EspSntp: {:?}", e))
}

pub fn wait_for_sntp_time_sync(sntp: &EspSntp) {
    while sntp.get_sync_status() != esp_idf_svc::sntp::SyncStatus::Completed {
        std::thread::sleep(Duration::from_millis(100));
    }
}

pub fn start_http_server(port: u16, settings: Arc<Mutex<Settings>>) -> Result<EspHttpServer> {
    let config = HttpConfiguration {
        http_port: port,
        ..Default::default()
    };

    let instance_name;
    if cfg!(feature = "tad") {
        instance_name = "(Tadeusz) ";
    } else if cfg!(feature = "wne") {
        instance_name = "(Wiktoria & Elżbieta) ";
    } else {
        instance_name = "";
    }

    let mut server = EspHttpServer::new(&config)?;

    let settings_get = settings.clone();
    let settings_post = settings;

    server.handle_get("/", move |_req, resp| {
        let settings = settings_get.lock()?;
        resp.send_str(format!(r#"
        <doctype html5>
        <html>
        <head>
            <meta charset="UTF-8">
        </head>
        <body>
        <form method = "post" action = "/update_settings" enctype="application/x-www-form-urlencoded" class="form-horizontal">
        <fieldset>

        <legend>TadKeep {}Settings</legend>

        <div class="form-group">
        <label class="col-md-4 control-label" for="time_mode">Time Mode</label>
        <div class="col-md-4">
            <select id="time_mode" name="time_mode" value="{}" class="form-control">
            <option value="AUTO">AUTO</option>
            <option value="MANUAL">MANUAL</option>
            </select>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="sunrise">Sunrise</label>
        <div class="col-md-4">
            <input type="time" name="sunrise" value="{}">
            <span class="validity"></span>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="sunset">Sunset</label>
        <div class="col-md-4">
            <input type="time" name="sunset" value="{}">
            <span class="validity"></span>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="solar_mode">Solar Mode</label>
        <div class="col-md-4">
            <select id="solar_mode" name="solar_mode" value="{}" class="form-control">
            <option value="AUTO">AUTO</option>
            <option value="ON">ON</option>
            <option value="OFF">OFF</option>
            </select>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="uvb_mode">UVB Mode</label>
        <div class="col-md-4">
            <select id="uvb_mode" name="uvb_mode" value="{}" class="form-control">
            <option value="AUTO">AUTO</option>
            <option value="ON">ON</option>
            <option value="OFF">OFF</option>
            </select>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="warm_led_mode">Warm LEDs Mode</label>
        <div class="col-md-4">
            <select id="warm_led_mode" name="warm_led_mode" value="{}" class="form-control">
            <option value="AUTO">AUTO</option>
            <option value="ON">ON</option>
            <option value="OFF">OFF</option>
            </select>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="cool_led_mode">Cool LEDs Mode</label>
        <div class="col-md-4">
            <select id="cool_led_mode" name="cool_led_mode" value="{}" class="form-control">
            <option value="AUTO">AUTO</option>
            <option value="ON">ON</option>
            <option value="OFF">OFF</option>
            </select>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="heat_mode">Heat Mode</label>
        <div class="col-md-4">
            <select id="heat_mode" name="heat_mode" value="{}" class="form-control">
            <option value="AUTO">AUTO</option>
            <option value="ON">ON</option>
            <option value="OFF">OFF</option>
            </select>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="max_day_warm_scale">max_day_warm_scale</label>  
        <div class="col-md-4">
        <input id="max_day_warm_scale" name="max_day_warm_scale" type="text" value="{:.02}" class="form-control input-md" required="">
            
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="max_day_cool_scale">max_day_cool_scale</label>  
        <div class="col-md-4">
        <input id="max_day_cool_scale" name="max_day_cool_scale" type="text" value="{:.02}" class="form-control input-md" required="">
            
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="max_night_scale">max_night_scale</label>  
        <div class="col-md-4">
        <input id="max_night_scale" name="max_night_scale" type="text" value="{:.02}" class="form-control input-md" required="">
            
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label" for="savebutton"></label>
        <div class="col-md-4">
            <button id="savebutton" name="savebutton" class="btn btn-primary">Save</button>
        </div>
        </div>

        </fieldset>
        </form>
        </body>
        </html>
        "#, instance_name, settings.time_mode, settings.sunpair.sunrise.format("%H:%M"), settings.sunpair.sunset.format("%H:%M"), settings.solar_mode, settings.uvb_mode, settings.warm_led_mode, settings.cool_led_mode, settings.heat_mode, settings.max_day_warm_scale, settings.max_day_cool_scale, settings.max_night_scale).as_str())?;

        Ok(())
    })?
    .handle_post("/update_settings", move |mut req, resp| {
        let mut body = Vec::new();

        ToStd::new(req.reader()).read_to_end(&mut body)?;

        let time_mode = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "time_mode"})
            .map(|p|  str::parse::<TimeMode>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No time_mode value!"))??;

        let sunrise = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "sunrise"})
            .map(|p|   chrono::NaiveTime::parse_from_str(&p.1, "%H:%M").map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No sunrise value!"))??;

        let sunset = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "sunset"})
            .map(|p|  chrono::NaiveTime::parse_from_str(&p.1, "%H:%M").map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No sunset value!"))??;

        let solar_mode = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "solar_mode"})
            .map(|p|  str::parse::<Mode>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No solar_mode value!"))??;

        let uvb_mode = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "uvb_mode"})
            .map(|p|  str::parse::<Mode>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No uvb_mode value!"))??;

        let warm_led_mode = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "warm_led_mode"})
            .map(|p|  str::parse::<Mode>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No warm_led_mode value!"))??;

        let cool_led_mode = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "cool_led_mode"})
            .map(|p|  str::parse::<Mode>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No cool_led_mode value!"))??;

        let heat_mode = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "heat_mode"})
            .map(|p|  str::parse::<Mode>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No heat_mode value!"))??;

        let max_day_warm_scale = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "max_day_warm_scale"})
            .map(|p|  str::parse::<f32>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No max_day_warm_scale value!"))??;

        let max_day_cool_scale = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "max_day_cool_scale"})
            .map(|p|  str::parse::<f32>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No max_day_cool_scale value!"))??;

        let max_night_scale = url::form_urlencoded::parse(&body)
            .filter(|p| {p.0 == "max_night_scale"})
            .map(|p|  str::parse::<f32>(&p.1).map_err(anyhow::Error::msg))
            .next()
            .ok_or_else(|| anyhow::anyhow!("No max_night_scale value!"))??;

        let mut settings = settings_post.lock()?;

        settings.time_mode = time_mode;
        settings.sunpair.sunrise = sunrise;
        settings.sunpair.sunset = sunset;
        settings.solar_mode = solar_mode;
        settings.uvb_mode = uvb_mode;
        settings.warm_led_mode = warm_led_mode;
        settings.cool_led_mode = cool_led_mode;
        settings.heat_mode = heat_mode;
        settings.max_day_warm_scale = max_day_warm_scale;
        settings.max_day_cool_scale = max_day_cool_scale;
        settings.max_night_scale = max_night_scale;

        resp.send_str(
            &format!(
            r#"
            <doctype html5>
            <html>
                <body>
                    Settings saved:<br>
                    time_mode = {}<br>
                    sunrise = {}<br>
                    sunset = {}<br>
                    solar_mode = {}<br>
                    uvb_mode = {}<br>
                    warm_led_mode = {}<br>
                    cool_led_mode = {}<br>
                    heat_mode = {}<br>
                    max_day_warm_scale = {:.02}<br>
                    max_day_cool_scale = {:.02}<br>
                    max_night_scale = {:.02}
                </body>
            </html>
            "#, time_mode, sunrise, sunset, solar_mode, uvb_mode, warm_led_mode, cool_led_mode, heat_mode, max_day_warm_scale, max_day_cool_scale, max_night_scale))?;
        Ok(())
    })?;

    Ok(server)
}
