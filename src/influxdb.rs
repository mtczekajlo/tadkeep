#[cfg(feature = "metrics")]
use anyhow::{Error, Result};
#[cfg(feature = "metrics")]
use embedded_svc::http::{
    client::{Client, Request},
    Method, SendHeaders,
};
#[cfg(feature = "metrics")]
use esp_idf_svc::http::client::{EspHttpClient, EspHttpRequest};

#[cfg(feature = "metrics")]
use crate::esp_time::Rtc;

#[cfg(feature = "metrics")]
const INFLUXDB_HOST: &str = env!("INFLUX_HOST_PORT");
#[cfg(feature = "metrics")]
const INFLUXDB_ORG: &str = "home";

#[cfg(feature = "tad")]
const INFLUXDB_BUCKET: &str = "tad";
#[cfg(feature = "wne")]
const INFLUXDB_BUCKET: &str = "wne";

#[cfg(feature = "tad")]
const INFLUXDB_TOKEN: &str = env!("TOKEN_TAD");
#[cfg(feature = "wne")]
const INFLUXDB_TOKEN: &str = env!("TOKEN_WNE");

#[cfg(feature = "tad")]
pub fn post_tad_records(
    hot_zone_temperature: f32,
    hot_zone_humidity: f32,
    cold_zone_temperature: f32,
    cold_zone_humidity: f32,
) -> Result<(), Error> {
    let timestamp = Rtc.get_utc_datetime().timestamp();

    if vec![
        hot_zone_temperature,
        hot_zone_humidity,
        cold_zone_temperature,
        cold_zone_humidity,
    ]
    .iter()
    .all(|el| *el == 0.0)
    {
        let err_msg = "No valid measurements. Skipping data posting.";
        println!("{err_msg}");
        return Err(anyhow::anyhow!(err_msg));
    }

    let mut content = String::new();

    if hot_zone_temperature != 0.0 {
        content =
            format!("{content}hot_zone_temperature value={hot_zone_temperature:.3} {timestamp}\n");
    }

    if hot_zone_humidity != 0.0 {
        content = format!("{content}hot_zone_humidity value={hot_zone_humidity:.3} {timestamp}\n");
    }

    if cold_zone_temperature != 0.0 {
        content = format!(
            "{content}cold_zone_temperature value={cold_zone_temperature:.3} {timestamp}\n"
        );
    }

    if cold_zone_humidity != 0.0 {
        content =
            format!("{content}cold_zone_humidity value={cold_zone_humidity:.3} {timestamp}\n");
    }

    println!("Content: {:?}", content);

    post_content(&content)
}

#[cfg(feature = "wne")]
pub fn post_wne_records(heat_mat_temperature: f32, heat_mat_state: bool) -> Result<(), Error> {
    let timestamp = Rtc.get_utc_datetime().timestamp();

    let mut content = String::new();

    if heat_mat_temperature != 0.0 {
        content =
            format!("{content}heat_mat_temperature value={heat_mat_temperature:.3} {timestamp}\n");
    }

    content = format!("{content}heat_mat_state value={heat_mat_state} {timestamp}\n");

    post_content(&content)
}

#[cfg(any(feature = "tad", feature = "wne"))]
fn prepare_request<'a>(
    url: &str,
    client: &'a mut EspHttpClient,
) -> Result<EspHttpRequest<'a>, Error> {
    let mut request = client.request(Method::Post, url)?;
    request.set_header(
        "Authorization",
        format!("Token {}", INFLUXDB_TOKEN).as_str(),
    );
    request.set_header("Accept", "application/json");
    request.set_header("Content-Type", "text/plain; charset=utf-8");
    Ok(request)
}

#[cfg(any(feature = "tad", feature = "wne"))]
fn post_content(content: &str) -> Result<(), Error> {
    let url = format!(
        "http://{}/api/v2/write?org={}&bucket={}&precision=s",
        INFLUXDB_HOST, INFLUXDB_ORG, INFLUXDB_BUCKET,
    );
    let mut client = EspHttpClient::new_default()?;

    let request = prepare_request(&url, &mut client)?;

    request.send_bytes(content.as_bytes())?;

    client
        .post(&url)?
        .submit()
        .map_err(|e| anyhow::anyhow!("HTTP POST error: {:?}", e))?;

    Ok(())
}
