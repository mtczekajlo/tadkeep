use libm::powf;

pub fn apply_gamma_correction<T>(value: T, gamma: f32) -> Option<T>
where
    T: num_traits::ToPrimitive + num_traits::FromPrimitive + num_traits::Bounded,
{
    let float_value = value.to_f32()? / T::max_value().to_f32()?;
    T::from_f32(powf(float_value, gamma) * (T::max_value().to_f32()? + 0.5f32))
}

pub fn scale_value<T>(value: T, scale: f32) -> Option<T>
where
    T: num_traits::ToPrimitive + num_traits::FromPrimitive,
{
    T::from_f32(value.to_f32()? * scale)
}
