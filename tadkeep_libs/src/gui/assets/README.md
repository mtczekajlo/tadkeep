# Assets

## Tools

- [Pixelorama](https://orama-interactive.itch.io/pixelorama) -> [release page](https://github.com/Orama-Interactive/Pixelorama/releases)
- [PiskelApp](https://www.piskelapp.com/) -> [release page](https://github.com/piskelapp/piskel/releases)
- [convert](https://imagemagick.org/index.php) -> `sudo dnf install ImageMagick`

## Spells

See `convert_images` script.
