pub const CLOCK_ON_RAW_DATA: &[u8] = include_bytes!("clock_on_15x15.png.raw");
pub const CLOCK_OFF_RAW_DATA: &[u8] = include_bytes!("clock_off_15x15.png.raw");
pub const CLOCK_ERR_RAW_DATA: &[u8] = include_bytes!("clock_err_15x15.png.raw");
pub const WIFI_ON_RAW_DATA: &[u8] = include_bytes!("wifi_on_15x15.png.raw");
pub const WIFI_OFF_RAW_DATA: &[u8] = include_bytes!("wifi_off_15x15.png.raw");
pub const WIFI_ERR_RAW_DATA: &[u8] = include_bytes!("wifi_err_15x15.png.raw");
pub const THERMOMETER_ON_COLD_RAW_DATA: &[u8] = include_bytes!("thermometer_on_cold_15x15.png.raw");
pub const THERMOMETER_OFF_COLD_RAW_DATA: &[u8] =
    include_bytes!("thermometer_off_cold_15x15.png.raw");
pub const THERMOMETER_ERR_COLD_RAW_DATA: &[u8] =
    include_bytes!("thermometer_err_cold_15x15.png.raw");
pub const THERMOMETER_ON_HOT_RAW_DATA: &[u8] = include_bytes!("thermometer_on_hot_15x15.png.raw");
pub const THERMOMETER_OFF_HOT_RAW_DATA: &[u8] = include_bytes!("thermometer_off_hot_15x15.png.raw");
pub const THERMOMETER_ERR_HOT_RAW_DATA: &[u8] = include_bytes!("thermometer_err_hot_15x15.png.raw");
pub const THERMOMETER_ON_GROUND_RAW_DATA: &[u8] =
    include_bytes!("thermometer_on_ground_15x15.png.raw");
pub const THERMOMETER_OFF_GROUND_RAW_DATA: &[u8] =
    include_bytes!("thermometer_off_ground_15x15.png.raw");
pub const THERMOMETER_ERR_GROUND_RAW_DATA: &[u8] =
    include_bytes!("thermometer_err_ground_15x15.png.raw");
