use crate::data::{Data, ElementStatus};
use anyhow::Error;
use chrono::NaiveDateTime;
use core::default::Default;
use embedded_graphics::{
    image::{Image, ImageRaw},
    mono_font::MonoTextStyle,
    pixelcolor::BinaryColor,
    prelude::*,
    text::*,
};

pub mod assets;

#[derive(Default)]
pub struct Gui {
    pub texts: [String; 5],
}

impl Gui {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    fn set_text(&mut self, i: usize, format: String) {
        let i = num_traits::clamp(i, 0, self.texts.len());
        self.texts[i] = format;
    }

    pub fn draw_gui<D>(
        &mut self,
        display: &mut D,
        data: &Data,
        datetime: NaiveDateTime,
    ) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        self.draw_icons(display, data)?;
        self.draw_datetime(display, datetime)?;
        self.populate_texts(data);
        self.draw_texts(display)
    }

    fn draw_datetime<D>(&mut self, display: &mut D, datetime: NaiveDateTime) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let newtime = datetime.format("%H:%M:%S");
        let newdate = datetime.format("%d.%m.%y");
        Text::with_text_style(
            newtime.to_string().as_str(),
            Point::new(128, 5),
            MonoTextStyle::new(&profont::PROFONT_7_POINT, BinaryColor::On),
            TextStyleBuilder::new().alignment(Alignment::Right).build(),
        )
        .draw(display)
        .map_err(|_| anyhow::anyhow!("Display error."))?;

        Text::with_text_style(
            newdate.to_string().as_str(),
            Point::new(128, 5 + 7),
            MonoTextStyle::new(&profont::PROFONT_7_POINT, BinaryColor::On),
            TextStyleBuilder::new().alignment(Alignment::Right).build(),
        )
        .draw(display)
        .map_err(|_| anyhow::anyhow!("Display error."))?;
        Ok(())
    }

    fn draw_clock<D>(&self, display: &mut D, icon_position: i32, data: &Data) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let clock_raw_image = match &data.clock {
            ElementStatus::On => ImageRaw::<BinaryColor>::new(assets::CLOCK_ON_RAW_DATA, 15),
            ElementStatus::Off => ImageRaw::<BinaryColor>::new(assets::CLOCK_OFF_RAW_DATA, 15),
            ElementStatus::Error => ImageRaw::<BinaryColor>::new(assets::CLOCK_ERR_RAW_DATA, 15),
        };
        let clock_image = Image::new(&clock_raw_image, Point::new(icon_position, 0));
        clock_image
            .draw(display)
            .map_err(|_| anyhow::anyhow!("Display error."))
    }

    fn draw_wifi<D>(&self, display: &mut D, icon_position: i32, data: &Data) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let wifi_raw_image = match &data.wifi {
            ElementStatus::On => ImageRaw::<BinaryColor>::new(assets::WIFI_ON_RAW_DATA, 15),
            ElementStatus::Off => ImageRaw::<BinaryColor>::new(assets::WIFI_OFF_RAW_DATA, 15),
            ElementStatus::Error => ImageRaw::<BinaryColor>::new(assets::WIFI_ERR_RAW_DATA, 15),
        };
        let wifi_image = Image::new(&wifi_raw_image, Point::new(icon_position, 0));
        wifi_image
            .draw(display)
            .map_err(|_| anyhow::anyhow!("Display error."))
    }

    #[cfg(feature = "tad")]
    fn draw_cold_thermometer<D>(
        &self,
        display: &mut D,
        icon_position: i32,
        data: &Data,
    ) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let thermometer_raw_image = match &data.cold_zone.sensor {
            ElementStatus::On => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_ON_COLD_RAW_DATA, 15)
            }
            ElementStatus::Off => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_OFF_COLD_RAW_DATA, 15)
            }
            ElementStatus::Error => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_ERR_COLD_RAW_DATA, 15)
            }
        };
        let thermometer_image = Image::new(&thermometer_raw_image, Point::new(icon_position, 0));
        thermometer_image
            .draw(display)
            .map_err(|_| anyhow::anyhow!("Display error."))
    }

    #[cfg(feature = "tad")]
    fn draw_hot_thermometer<D>(
        &self,
        display: &mut D,
        icon_position: i32,
        data: &Data,
    ) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let thermometer_raw_image = match &data.hot_zone.sensor {
            ElementStatus::On => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_ON_HOT_RAW_DATA, 15)
            }
            ElementStatus::Off => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_OFF_HOT_RAW_DATA, 15)
            }
            ElementStatus::Error => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_ERR_HOT_RAW_DATA, 15)
            }
        };
        let thermometer_image = Image::new(&thermometer_raw_image, Point::new(icon_position, 0));
        thermometer_image
            .draw(display)
            .map_err(|_| anyhow::anyhow!("Display error."))
    }

    #[cfg(feature = "wne")]
    fn draw_ground_thermometer<D>(
        &self,
        display: &mut D,
        icon_position: i32,
        data: &Data,
    ) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let thermometer_raw_image = match &data.heat_mat.sensor {
            ElementStatus::On => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_ON_GROUND_RAW_DATA, 15)
            }
            ElementStatus::Off => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_OFF_GROUND_RAW_DATA, 15)
            }
            ElementStatus::Error => {
                ImageRaw::<BinaryColor>::new(assets::THERMOMETER_ERR_GROUND_RAW_DATA, 15)
            }
        };
        let thermometer_image = Image::new(&thermometer_raw_image, Point::new(icon_position, 0));
        thermometer_image
            .draw(display)
            .map_err(|_| anyhow::anyhow!("Display error."))
    }

    fn populate_texts(&mut self, data: &Data) {
        self.set_text(
            0,
            format!(
                "W:{:3.0}% C:{:3.0}% SOL:{} UVB:{}",
                data.lights.warm_leds * 100.0,
                data.lights.cool_leds * 100.0,
                data.lights.solar as u32,
                data.lights.uvb as u32,
            ),
        );

        #[cfg(feature = "wne")]
        {
            self.set_text(
                1,
                format!(
                    "HEAT MAT | {:4.01}°C   {}",
                    data.heat_mat.temperature,
                    if data.heat_mat.state { "ON" } else { "OFF" }
                ),
            );
        }

        #[cfg(feature = "tad")]
        {
            self.set_text(
                1,
                format!(
                    " HOT ZONE | {:4.01}°C {:4.01}%",
                    data.hot_zone.temperature, data.hot_zone.humidity
                ),
            );

            self.set_text(
                2,
                format!(
                    "COLD ZONE | {:4.01}°C {:4.01}%",
                    data.cold_zone.temperature, data.cold_zone.humidity
                ),
            );
        }

        self.set_text(
            3,
            format!(
                "MODE | {}",
                match data.sunpair_source {
                    crate::data::SunPairSource::Fetch | crate::data::SunPairSource::Model =>
                        format!("AUTO | {}", data.sunpair_auto.date.format("%d.%m.%Y")),
                    crate::data::SunPairSource::Manual => "MANUAL".to_string(),
                }
            ),
        );
        match data.sunpair_source {
            crate::data::SunPairSource::Fetch | crate::data::SunPairSource::Model => self.set_text(
                4,
                format!(
                    "{} | {}-{}",
                    data.sunpair_source,
                    data.sunpair_auto.sunrise.format("%H:%M:%S"),
                    data.sunpair_auto.sunset.format("%H:%M:%S")
                ),
            ),
            crate::data::SunPairSource::Manual => self.set_text(
                4,
                format!(
                    "{}-{}",
                    data.sunpair_manual.sunrise.format("%H:%M"),
                    data.sunpair_manual.sunset.format("%H:%M")
                ),
            ),
        }
    }

    fn draw_texts<D>(&self, display: &mut D) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let font = profont::PROFONT_7_POINT;
        for (i, text) in self.texts.iter().enumerate() {
            Text::with_text_style(
                text.as_str(),
                Point::new(
                    2,
                    16 + (i as i32 + 1) * ((font.character_size.height - 2) as i32),
                ),
                MonoTextStyle::new(&font, BinaryColor::On),
                TextStyleBuilder::new().alignment(Alignment::Left).build(),
            )
            .draw(display)
            .map_err(|_| anyhow::anyhow!("Display error."))?;
        }
        Ok(())
    }

    fn draw_icons<D>(&self, display: &mut D, data: &Data) -> Result<(), Error>
    where
        D: DrawTarget<Color = BinaryColor>,
    {
        let mut icon_positions = [0, 16, 32, 48, 64, 80].iter();
        self.draw_clock(display, *icon_positions.next().unwrap(), data)?;
        self.draw_wifi(display, *icon_positions.next().unwrap(), data)?;
        #[cfg(feature = "tad")]
        {
            self.draw_hot_thermometer(display, *icon_positions.next().unwrap(), data)?;
            self.draw_cold_thermometer(display, *icon_positions.next().unwrap(), data)?;
        }
        #[cfg(feature = "wne")]
        {
            self.draw_ground_thermometer(display, *icon_positions.next().unwrap(), data)?;
        }
        Ok(())
    }
}
