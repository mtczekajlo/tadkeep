use crate::sunrise_sunset::SunPair;
use anyhow::Result;
use core::default::Default;
use enum_iterator::Sequence;
use std::{fmt, slice::Iter};

#[derive(Default, Sequence, Clone)]
pub enum ElementStatus {
    #[default]
    Off,
    On,
    Error,
}

impl ElementStatus {
    pub fn from<T>(result: &Option<Result<T>>) -> ElementStatus {
        match result {
            Some(Ok(_)) => ElementStatus::On,
            Some(Err(_)) => ElementStatus::Error,
            None => ElementStatus::Off,
        }
    }

    pub fn iterator() -> Iter<'static, ElementStatus> {
        static STATUSES: [ElementStatus; 3] =
            [ElementStatus::Off, ElementStatus::On, ElementStatus::Error];
        STATUSES.iter()
    }
}

impl fmt::Display for ElementStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ElementStatus::Off => write!(f, "OFF"),
            ElementStatus::On => write!(f, "ON"),
            ElementStatus::Error => write!(f, "ERR"),
        }
    }
}

#[derive(Default)]
pub struct Zone {
    pub sensor: ElementStatus,
    pub temperature: f32,
    pub humidity: f32,
}

#[derive(Default)]
pub struct HeatMat {
    pub sensor: ElementStatus,
    pub state: bool,
    pub temperature: f32,
}

#[derive(Default)]
pub struct Lights {
    pub solar: bool,
    pub uvb: bool,
    pub warm_leds: f32,
    pub cool_leds: f32,
}

#[derive(Default, Sequence, Clone, PartialEq)]
pub enum SunPairSource {
    #[default]
    Model,
    Fetch,
    Manual,
}

impl SunPairSource {
    pub fn iterator() -> Iter<'static, SunPairSource> {
        static SOURCES: [SunPairSource; 2] = [SunPairSource::Model, SunPairSource::Fetch];
        SOURCES.iter()
    }
}

impl fmt::Display for SunPairSource {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SunPairSource::Model => write!(f, "MODEL"),
            SunPairSource::Fetch => write!(f, "FETCH"),
            SunPairSource::Manual => write!(f, "MANUAL"),
        }
    }
}

#[derive(Default)]
pub struct Data {
    pub wifi: ElementStatus,
    pub clock: ElementStatus,
    pub cold_zone: Zone,
    pub hot_zone: Zone,
    pub lights: Lights,
    pub heat_mat: HeatMat,
    pub sunpair_auto: SunPair,
    pub sunpair_manual: SunPair,
    pub sunpair_source: SunPairSource,
}

impl Data {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }
}

impl fmt::Display for Data {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "WiFi: {} | Clock: {} | Hot: {} | Cold: {} | Heat: {}",
            self.wifi,
            self.clock,
            self.hot_zone.sensor,
            self.cold_zone.sensor,
            self.heat_mat.sensor
        )?;

        writeln!(
            f,
            "Warm LEDs: {:5.02}% | Cold LEDs: {:5.02}% | Solar: {} | UVB: {} | Heat: {}",
            self.lights.warm_leds * 100.0,
            self.lights.cool_leds * 100.0,
            self.lights.solar,
            self.lights.uvb,
            self.heat_mat.state,
        )?;

        writeln!(
            f,
            " HOT ZONE | {:5.02}°C {:5.02}%",
            self.hot_zone.temperature, self.hot_zone.humidity
        )?;

        writeln!(
            f,
            "COLD ZONE | {:5.02}°C {:5.02}%",
            self.cold_zone.temperature, self.cold_zone.humidity
        )?;

        writeln!(
            f,
            " HEAT MAT | {:5.02}°C  {:?}",
            self.heat_mat.temperature, self.heat_mat.state
        )?;

        write!(
            f,
            "MODE | {}",
            match self.sunpair_source {
                SunPairSource::Fetch | SunPairSource::Model => format!(
                    "{} | {} {}-{}",
                    self.sunpair_source,
                    self.sunpair_auto.date.format("%d.%m.%Y"),
                    self.sunpair_auto.sunrise.format("%H:%M:%S"),
                    self.sunpair_auto.sunset.format("%H:%M:%S")
                ),
                SunPairSource::Manual => format!(
                    "{} | {}-{}",
                    self.sunpair_source,
                    self.sunpair_manual.sunrise.format("%H:%M"),
                    self.sunpair_manual.sunset.format("%H:%M")
                ),
            }
        )?;
        Ok(())
    }
}
