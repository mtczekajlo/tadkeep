use chrono::Datelike;
use serde::{Serialize, Deserialize};
use core::convert::Into;
use core::default::Default;
use std::fmt;

#[derive(Clone)]
pub struct Place {
    pub latitude: f32,
    pub longitude: f32,
    pub timezone: &'static chrono_tz::Tz,
}

pub const WROCLAW: Place = Place {
    latitude: 51.107_88,
    longitude: 17.038_53,
    timezone: &chrono_tz::Europe::Warsaw,
}; // Wrocław, Poland

pub const AUSTRALIA: Place = Place {
    latitude: -27.370_58,
    longitude: 141.383_29,
    timezone: &chrono_tz::Australia::Melbourne,
}; // Durham, Australia

pub const IRAN: Place = Place {
    latitude: 35.715_29,
    longitude: 51.404_34,
    timezone: &chrono_tz::Asia::Tehran,
}; // Tehran, Iran

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct SunPair {
    pub date: chrono::NaiveDate,
    pub sunrise: chrono::NaiveTime,
    pub sunset: chrono::NaiveTime,
}

impl Default for SunPair {
    fn default() -> Self {
        Self {
            date: chrono::NaiveDate::from_ymd(1970, 1, 1),
            sunrise: chrono::NaiveTime::from_hms(0, 0, 0),
            sunset: chrono::NaiveTime::from_hms(0, 0, 0),
        }
    }
}

impl fmt::Display for SunPair {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{} {}", self.sunrise, self.sunset, self.date)
    }
}

pub fn get_default_model(place: Place, date: chrono::NaiveDate) -> SunPair {
    get_sunrise_sunset_model(place, date)
}

pub fn get_sunrise_sunset_model(place: Place, date: chrono::NaiveDate) -> SunPair {
    let (sunrise, sunset) = sunrise_lite::sunrise_sunset(
        place.latitude.into(),
        place.longitude.into(),
        date.year(),
        date.month(),
        date.day(),
    );
    let sunrise_naivedatetime = chrono::NaiveDateTime::from_timestamp(sunrise, 0);
    let sunset_naivedatetime = chrono::NaiveDateTime::from_timestamp(sunset, 0);
    let sunrise_datetime =
        chrono::DateTime::<chrono::Utc>::from_utc(sunrise_naivedatetime, chrono::Utc)
            .with_timezone(place.timezone);
    let sunset_datetime =
        chrono::DateTime::<chrono::Utc>::from_utc(sunset_naivedatetime, chrono::Utc)
            .with_timezone(place.timezone);

    SunPair {
        date: sunrise_datetime.naive_local().date(),
        sunrise: sunrise_datetime.naive_local().time(),
        sunset: sunset_datetime.naive_local().time(),
    }
}
