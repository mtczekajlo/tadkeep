use serde::{Deserialize, Serialize};
use std::{fmt, str};

use crate::sunrise_sunset::SunPair;

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Default)]
pub enum TimeMode {
    #[default]
    Auto,
    Manual,
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Default)]
pub enum Mode {
    #[default]
    Auto,
    On,
    Off,
}

impl str::FromStr for Mode {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "AUTO" => Ok(Mode::Auto),
            "ON" => Ok(Mode::On),
            "OFF" => Ok(Mode::Off),
            _ => Err(anyhow::anyhow!("Mode FromStr parse error")),
        }
    }
}

impl str::FromStr for TimeMode {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "AUTO" => Ok(TimeMode::Auto),
            "MANUAL" => Ok(TimeMode::Manual),
            _ => Err(anyhow::anyhow!("TimeMode FromStr parse error")),
        }
    }
}

impl fmt::Display for TimeMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TimeMode::Auto => "AUTO",
                TimeMode::Manual => "MANUAL",
            }
        )
    }
}

impl fmt::Display for Mode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Mode::Auto => "AUTO",
                Mode::On => "ON",
                Mode::Off => "OFF",
            }
        )
    }
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct Settings {
    pub time_mode: TimeMode,
    pub sunpair: SunPair,
    pub solar_mode: Mode,
    pub uvb_mode: Mode,
    pub warm_led_mode: Mode,
    pub cool_led_mode: Mode,
    pub heat_mode: Mode,
    pub max_day_warm_scale: f32,
    pub max_day_cool_scale: f32,
    pub max_night_scale: f32,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            time_mode: TimeMode::Auto,
            sunpair: Default::default(),
            solar_mode: Mode::Auto,
            uvb_mode: Mode::Auto,
            warm_led_mode: Mode::Auto,
            cool_led_mode: Mode::Auto,
            heat_mode: Mode::Auto,
            max_day_warm_scale: 1.0,
            max_day_cool_scale: 1.0,
            max_night_scale: 0.1,
        }
    }
}

impl Settings {
    pub fn to_bytes(&self) -> Result<Vec<u8>, bincode::Error> {
        bincode::serialize(&self)
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Self, bincode::Error> {
        bincode::deserialize(&bytes)
    }
}

impl fmt::Display for Settings {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Time:{} Sunrise:{} Sunset:{} Solar:{} UVB:{} Heat:{} Warm:{}/{:.02} Cool:{}/{:.02} Night:{:.02}",
            self.time_mode,
            self.sunpair.sunrise,
            self.sunpair.sunset,
            self.solar_mode,
            self.uvb_mode,
            self.warm_led_mode,
            self.cool_led_mode,
            self.heat_mode,
            self.max_day_warm_scale,
            self.max_day_cool_scale,
            self.max_night_scale
        )
    }
}
