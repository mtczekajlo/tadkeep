use chrono::{naive::NaiveTime, Duration, Timelike};

pub struct DawnDusk {
    pub on: NaiveTime,
    pub off: NaiveTime,
}

pub struct DawnDuskSmooth {
    pub turning_on: NaiveTime,
    pub fully_on: NaiveTime,
    pub turning_off: NaiveTime,
    pub fully_off: NaiveTime,
    pub is_nocturnal: bool,
    pub max_brightness_scale: f32,
}

impl DawnDusk {
    pub fn new(on: NaiveTime, off: NaiveTime) -> Self {
        Self { on, off }
    }

    pub fn is_on(&self, time: NaiveTime) -> bool {
        (self.on..self.off).contains(&time)
    }
}

impl DawnDuskSmooth {
    pub fn new_day(fully_on: NaiveTime, turning_off: NaiveTime, transition: Duration) -> Self {
        Self {
            turning_on: fully_on - transition,
            fully_on,
            turning_off,
            fully_off: turning_off + transition,
            is_nocturnal: false,
            max_brightness_scale: 1f32,
        }
    }

    pub fn new_night(turning_on: NaiveTime, fully_off: NaiveTime, transition: Duration) -> Self {
        Self {
            turning_on,
            fully_on: turning_on + transition,
            turning_off: fully_off - transition,
            fully_off,
            is_nocturnal: true,
            max_brightness_scale: 1f32,
        }
    }

    pub fn with_max_brightness_scale(mut self, max_brightness_scale: f32) -> Self {
        self.max_brightness_scale = max_brightness_scale;
        self
    }

    pub fn get_brightness_scale(&self, time: NaiveTime) -> f32 {
        let v = if self.is_nocturnal {
            self.get_brightness_scale_night(time)
        } else {
            self.get_brightness_scale_day(time)
        };
        v * self.max_brightness_scale
    }

    fn get_brightness_scale_day(&self, time: NaiveTime) -> f32 {
        if (NaiveTime::from_num_seconds_from_midnight(0, 0)..self.turning_on).contains(&time) {
            return 0f32;
        } else if (self.turning_on..self.fully_on).contains(&time) {
            return self.get_scale_from_time_range_dawn(time, self.turning_on, self.fully_on);
        } else if (self.fully_on..self.turning_off).contains(&time) {
            return 1f32;
        } else if (self.turning_off..=self.fully_off).contains(&time) {
            return self.get_scale_from_time_range_dusk(time, self.turning_off, self.fully_off);
        }
        0f32
    }

    fn get_brightness_scale_night(&self, time: NaiveTime) -> f32 {
        if (NaiveTime::from_num_seconds_from_midnight(0, 0)..self.turning_off).contains(&time) {
            return 1f32;
        } else if (self.turning_off..self.fully_off).contains(&time) {
            return self.get_scale_from_time_range_dusk(time, self.turning_off, self.fully_off);
        } else if (self.fully_off..self.turning_on).contains(&time) {
            return 0f32;
        } else if (self.turning_on..=self.fully_on).contains(&time) {
            return self.get_scale_from_time_range_dawn(time, self.turning_on, self.fully_on);
        }
        1f32
    }

    fn get_scale_from_time_range_dawn(
        &self,
        x: NaiveTime,
        start: NaiveTime,
        end: NaiveTime,
    ) -> f32 {
        let x = x.num_seconds_from_midnight();
        let start = start.num_seconds_from_midnight();
        let end = end.num_seconds_from_midnight();
        let a = (x - start) as f32;
        let b = (end - start) as f32;
        a / b
    }

    fn get_scale_from_time_range_dusk(
        &self,
        x: NaiveTime,
        start: NaiveTime,
        end: NaiveTime,
    ) -> f32 {
        let x = self.get_scale_from_time_range_dawn(x, start, end);
        1f32 - x
    }
}

pub struct DawnDuskTimes {
    pub solar: DawnDusk,
    pub uvb: DawnDusk,
    pub led_warm_day: DawnDuskSmooth,
    pub led_cool_day: DawnDuskSmooth,
    pub led_night: DawnDuskSmooth,
}

impl DawnDuskTimes {
    pub fn from_dawn_dusk(
        dawn: NaiveTime,
        dusk: NaiveTime,
        uvb_offset: Duration,
        led_transition: Duration,
    ) -> DawnDuskTimes {
        Self {
            solar: DawnDusk::new(dawn, dusk),
            uvb: DawnDusk::new(dawn + uvb_offset, dusk - uvb_offset),
            led_warm_day: DawnDuskSmooth::new_day(dawn, dusk, led_transition),
            led_cool_day: DawnDuskSmooth::new_day(
                dawn + uvb_offset,
                dusk - uvb_offset,
                led_transition,
            ),
            led_night: DawnDuskSmooth::new_night(
                dusk + uvb_offset,
                dawn - uvb_offset,
                led_transition,
            ),
        }
    }

    pub fn with_max_day_warm_brightness_scale(mut self, scale: f32) -> Self {
        self.led_warm_day.max_brightness_scale = scale;
        self
    }

    pub fn with_max_day_cool_brightness_scale(mut self, scale: f32) -> Self {
        self.led_cool_day.max_brightness_scale = scale;
        self
    }

    pub fn with_max_night_brightness_scale(mut self, scale: f32) -> Self {
        self.led_night.max_brightness_scale = scale;
        self
    }
}

#[cfg(test)]
mod tests {
    use super::{DawnDusk, DawnDuskSmooth};
    use chrono::{Duration, NaiveTime};
    use rstest::rstest;

    #[rstest]
    #[case(NaiveTime::from_hms(7, 59, 59), false)]
    #[case(NaiveTime::from_hms(8, 0, 0), true)]
    #[case(NaiveTime::from_hms(8, 0, 1), true)]
    #[case(NaiveTime::from_hms(19, 59, 59), true)]
    #[case(NaiveTime::from_hms(20, 0, 0), false)]
    #[case(NaiveTime::from_hms(20, 0, 1), false)]
    fn dawn_dusk(#[case] hms: NaiveTime, #[case] expected: bool) {
        let dd = DawnDusk::new(NaiveTime::from_hms(8, 0, 0), NaiveTime::from_hms(20, 0, 0));

        assert_eq!(dd.is_on(hms), expected);
    }

    #[rstest]
    #[case(NaiveTime::from_hms(7, 50, 0), 0.00f32)]
    #[case(NaiveTime::from_hms(7, 52, 30), 0.25f32)]
    #[case(NaiveTime::from_hms(7, 55, 0), 0.50f32)]
    #[case(NaiveTime::from_hms(7, 57, 30), 0.75f32)]
    #[case(NaiveTime::from_hms(8, 0, 0), 1.00f32)]
    #[case(NaiveTime::from_hms(20, 0, 0), 1.00f32)]
    #[case(NaiveTime::from_hms(20, 2, 30), 0.75f32)]
    #[case(NaiveTime::from_hms(20, 5, 0), 0.50f32)]
    #[case(NaiveTime::from_hms(20, 7, 30), 0.25f32)]
    #[case(NaiveTime::from_hms(20, 10, 0), 0.00f32)]
    fn dawn_dusk_smooth_day(#[case] hms: NaiveTime, #[case] expected: f32) {
        let dds = DawnDuskSmooth::new_day(
            NaiveTime::from_hms(8, 0, 0),
            NaiveTime::from_hms(20, 0, 0),
            Duration::minutes(10),
        );

        assert_eq!(dds.get_brightness_scale(hms), expected);
    }

    #[rstest]
    #[case(NaiveTime::from_hms(7, 50, 0), 1.00f32)]
    #[case(NaiveTime::from_hms(7, 52, 30), 0.75f32)]
    #[case(NaiveTime::from_hms(7, 55, 0), 0.50f32)]
    #[case(NaiveTime::from_hms(7, 57, 30), 0.25f32)]
    #[case(NaiveTime::from_hms(8, 0, 0), 0.00f32)]
    #[case(NaiveTime::from_hms(20, 0, 0), 0.00f32)]
    #[case(NaiveTime::from_hms(20, 2, 30), 0.25f32)]
    #[case(NaiveTime::from_hms(20, 5, 0), 0.50f32)]
    #[case(NaiveTime::from_hms(20, 7, 30), 0.75f32)]
    #[case(NaiveTime::from_hms(20, 10, 0), 1.00f32)]
    fn dawn_dusk_smooth_night(#[case] hms: NaiveTime, #[case] expected: f32) {
        let dds = DawnDuskSmooth::new_night(
            NaiveTime::from_hms(20, 0, 0),
            NaiveTime::from_hms(8, 0, 0),
            Duration::minutes(10),
        );

        assert_eq!(dds.get_brightness_scale(hms), expected);
    }

    #[rstest]
    #[case(NaiveTime::from_hms(7, 50, 0), 0.100f32)]
    #[case(NaiveTime::from_hms(7, 52, 30), 0.075f32)]
    #[case(NaiveTime::from_hms(7, 55, 0), 0.050f32)]
    #[case(NaiveTime::from_hms(7, 57, 30), 0.025f32)]
    #[case(NaiveTime::from_hms(8, 0, 0), 0.000f32)]
    #[case(NaiveTime::from_hms(20, 0, 0), 0.000f32)]
    #[case(NaiveTime::from_hms(20, 2, 30), 0.025f32)]
    #[case(NaiveTime::from_hms(20, 5, 0), 0.050f32)]
    #[case(NaiveTime::from_hms(20, 7, 30), 0.075f32)]
    #[case(NaiveTime::from_hms(20, 10, 0), 0.100f32)]
    fn dawn_dusk_smooth_night_limited(#[case] hms: NaiveTime, #[case] expected: f32) {
        let dds = DawnDuskSmooth::new_night(
            NaiveTime::from_hms(20, 0, 0),
            NaiveTime::from_hms(8, 0, 0),
            Duration::minutes(10),
        )
        .with_max_brightness_scale(0.1f32);

        assert_eq!(dds.get_brightness_scale(hms), expected);
    }
}
