use anyhow::Result;
use embedded_graphics::{pixelcolor::BinaryColor, prelude::*};
use embedded_graphics_simulator::{
    BinaryColorTheme, OutputSettingsBuilder, SimulatorDisplay, SimulatorEvent, Window,
};
use enum_iterator::next_cycle;
use rand::Rng;
use tadkeep_libs::{
    data::{Data, ElementStatus, SunPairSource},
    gui::{self},
    sunrise_sunset::{self, AUSTRALIA},
};

fn main() -> Result<()> {
    let mut display = SimulatorDisplay::<BinaryColor>::new(Size::new(128, 64));
    let output_settings = OutputSettingsBuilder::new()
        .theme(BinaryColorTheme::OledWhite)
        .build();
    let mut window = Window::new("TadKeep GUI", &output_settings);

    let mut data = Data::new();
    let mut gui = gui::Gui::new();
    let mut element_status = ElementStatus::Off;
    let mut element_bool = false;
    let mut sunpair_source = SunPairSource::Model;
    let mut rng = rand::thread_rng();
    'running: loop {
        let datetime = chrono::NaiveDateTime::new(
            chrono::NaiveDate::from_ymd(
                rng.gen_range(1970..2070),
                rng.gen_range(1..13),
                rng.gen_range(1..29), // #justFebruarythings
            ),
            chrono::NaiveTime::from_hms(
                rng.gen_range(0..24),
                rng.gen_range(0..60),
                rng.gen_range(0..60),
            ),
        );

        element_status = next_cycle(&element_status).unwrap();
        element_bool = element_bool ^ true;
        sunpair_source = next_cycle(&sunpair_source).unwrap();

        data.clock = element_status.clone();
        data.wifi = element_status.clone();
        data.hot_zone.sensor = element_status.clone();
        data.hot_zone.temperature = rng.gen_range(0f32..100f32);
        data.hot_zone.humidity = rng.gen_range(0f32..100f32);
        data.cold_zone.sensor = element_status.clone();
        data.cold_zone.temperature = rng.gen_range(0f32..100f32);
        data.cold_zone.humidity = rng.gen_range(0f32..100f32);
        data.lights.warm_leds = rng.gen_range(0f32..1f32);
        data.lights.cool_leds = rng.gen_range(0f32..1f32);
        data.lights.solar = rng.gen_bool(0.5);
        data.lights.uvb = rng.gen_bool(0.5);
        data.heat_mat.temperature = rng.gen_range(0f32..100f32);
        data.heat_mat.state = rng.gen_bool(0.5);
        data.sunpair_manual = sunrise_sunset::get_default_model(AUSTRALIA, datetime.date());
        data.sunpair_auto = sunrise_sunset::get_default_model(AUSTRALIA, datetime.date());
        data.sunpair_source = sunpair_source.clone();

        println!("{}", data);
        let _ = display.clear(BinaryColor::Off);
        let _ = gui.draw_gui(&mut display, &data, datetime);

        window.update(&display);
        if window.events().any(|e| e == SimulatorEvent::Quit) {
            break 'running Ok(());
        }
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
}
