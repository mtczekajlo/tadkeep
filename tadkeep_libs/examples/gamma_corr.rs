use tadkeep_libs::gamma_corr::*;

fn main() {
    for scale in (-5..110).step_by(5) {
        let scale = scale as f32 * 0.01;
        let duty = scale_value(u16::MAX, scale);
        if let Some(duty) = duty {
            let duty_corrected = apply_gamma_correction(duty, 2.8);
            let duty_corrected = duty_corrected.unwrap();
            println!(
                "{:+.02} -> {:5} -> {:5} (actual scale: {:.03})",
                scale,
                duty,
                duty_corrected,
                duty_corrected as f32 / u16::MAX as f32
            );
        } else {
            println!("{:+.02} -> {:?}", scale, duty);
        }
    }
}
