#[cfg(feature = "heat_mat")]
use ds18b20::*;
use eeprom24x::{Eeprom24x, SlaveAddr};
use embedded_graphics::{
    mono_font::MonoTextStyle,
    pixelcolor::BinaryColor,
    prelude::Point,
    text::{Alignment, Text, TextStyleBuilder},
    Drawable,
};
#[cfg(any(feature = "solar", feature = "uvb", feature = "heat_mat"))]
use embedded_hal::digital::v2::ToggleableOutputPin;
#[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
use esp_idf_hal::ledc::{config, Channel, Resolution as EspResolution, Timer};
use esp_idf_hal::prelude::*;
#[cfg(feature = "heat_mat")]
use one_wire_bus::*;
#[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
use tadkeep_libs::gamma_corr;
use tadkeep_libs::{
    ds323x::{DateTimeAccess, Ds323x},
    sh1106::prelude::GraphicsMode,
};

use esp_idf_sys as _;

fn main() {
    esp_idf_sys::link_patches();

    let peripherals = Peripherals::take().unwrap();
    #[cfg(any(feature = "zone_sensors", feature = "heat_mat"))]
    let mut delay = esp_idf_hal::delay::Ets;

    #[cfg(feature = "uvb")]
    let uvb_gpio = peripherals.pins.gpio5;
    #[cfg(feature = "heat_mat")]
    let heat_sensor_gpio = peripherals.pins.gpio16;
    #[cfg(feature = "heat_mat")]
    let heat_mat_gpio = peripherals.pins.gpio17;
    #[cfg(feature = "solar")]
    let solar_gpio = peripherals.pins.gpio18;
    #[cfg(feature = "warm_leds")]
    let led_warm_gpio = peripherals.pins.gpio19;
    #[cfg(feature = "cool_leds")]
    let led_cool_gpio = peripherals.pins.gpio21;
    let scl0 = peripherals.pins.gpio26;
    let sda0 = peripherals.pins.gpio27;
    let scl1 = peripherals.pins.gpio32;
    let sda1 = peripherals.pins.gpio33;

    #[cfg(feature = "heat_mat")]
    let heat_sensor_gpio = heat_sensor_gpio.into_input_output_od().unwrap();
    #[cfg(feature = "solar")]
    let mut solar_gpio = solar_gpio.into_input_output().unwrap();
    #[cfg(feature = "uvb")]
    let mut uvb_gpio = uvb_gpio.into_input_output().unwrap();
    #[cfg(feature = "heat_mat")]
    let mut heat_mat_gpio = heat_mat_gpio.into_input_output().unwrap();

    #[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
    let config = config::TimerConfig::default()
        .frequency(1.kHz().into())
        .resolution(EspResolution::Bits16);

    #[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
    let timer = Timer::new(peripherals.ledc.timer0, &config).unwrap();

    #[cfg(feature = "warm_leds")]
    let mut led_warm = Channel::new(peripherals.ledc.channel0, &timer, led_warm_gpio).unwrap();
    #[cfg(feature = "cool_leds")]
    let mut led_cool = Channel::new(peripherals.ledc.channel1, &timer, led_cool_gpio).unwrap();

    let config =
        <esp_idf_hal::i2c::config::MasterConfig as Default>::default().baudrate(400.kHz().into());

    let i2c0 = esp_idf_hal::i2c::Master::<esp_idf_hal::i2c::I2C0, _, _>::new(
        peripherals.i2c0,
        esp_idf_hal::i2c::MasterPins {
            sda: sda0,
            scl: scl0,
        },
        config,
    )
    .unwrap();

    let i2c0_shared = shared_bus::new_std!(esp_idf_hal::i2c::Master<esp_idf_hal::i2c::I2C0, esp_idf_hal::gpio::Gpio27<esp_idf_hal::gpio::Unknown>, esp_idf_hal::gpio::Gpio26<esp_idf_hal::gpio::Unknown>> = i2c0).unwrap();

    let mut display: GraphicsMode<_> = sh1106::Builder::new()
        .connect_i2c(i2c0_shared.acquire_i2c())
        .into();
    let display_result = display.init();
    println!("Display {:?}", display_result);
    display.clear();
    let _ = display.flush();
    let font = profont::PROFONT_7_POINT;

    let i2c1 = esp_idf_hal::i2c::Master::<esp_idf_hal::i2c::I2C1, _, _>::new(
        peripherals.i2c1,
        esp_idf_hal::i2c::MasterPins {
            sda: sda1,
            scl: scl1,
        },
        config,
    )
    .unwrap();

    let i2c1_shared = shared_bus::new_std!(esp_idf_hal::i2c::Master<esp_idf_hal::i2c::I2C1, esp_idf_hal::gpio::Gpio33<esp_idf_hal::gpio::Unknown>, esp_idf_hal::gpio::Gpio32<esp_idf_hal::gpio::Unknown>> = i2c1).unwrap();

    let mut external_rtc = Ds323x::new_ds3231(i2c1_shared.acquire_i2c());

    #[cfg(feature = "zone_sensors")]
    let mut shtc3_hot_zone = shtcx::shtc3(i2c1_shared.acquire_i2c());
    #[cfg(feature = "zone_sensors")]
    let mut shtc3_cold_zone = shtcx::shtc3(i2c0_shared.acquire_i2c());

    #[cfg(feature = "heat_mat")]
    let mut one_wire_bus = OneWire::new(heat_sensor_gpio).unwrap();

    let address = SlaveAddr::default();
    let mut eeprom = Eeprom24x::new_24x32(i2c1_shared.acquire_i2c(), address);

    loop {
        println!("==========");

        display.clear();
        let _ = display.flush();

        let dt = external_rtc.datetime();
        println!("EXT RTC:         {:?}", dt);

        let eeprom_data = eeprom.read_byte(0x0000);
        println!("EEPROM:          {:?}", eeprom_data);

        let _ = Text::with_text_style(
            format!("{:?}", dt).as_str(),
            Point::new(2, 16),
            MonoTextStyle::new(&font, BinaryColor::On),
            TextStyleBuilder::new().alignment(Alignment::Left).build(),
        )
        .draw(&mut display);
        let flush_result = display.flush();
        println!("DISPLAY FLUSH:   {:?}", flush_result);

        #[cfg(feature = "solar")]
        {
            let gpio_result = solar_gpio.toggle();
            println!("SOLAR:           {:?}", gpio_result);
        }

        #[cfg(feature = "uvb")]
        {
            let gpio_result = uvb_gpio.toggle();
            println!("UVB:             {:?}", gpio_result);
        }

        #[cfg(feature = "heat_mat")]
        {
            let gpio_result = heat_mat_gpio.toggle();
            println!("HEAT MAT:        {:?}", gpio_result);
        }

        #[cfg(feature = "zone_sensors")]
        {
            let shtc_meas = shtc3_hot_zone.measure(shtcx::PowerMode::NormalMode, &mut delay);
            println!("HOT SHTC3:       {:?}", shtc_meas);
            let shtc_meas = shtc3_cold_zone.measure(shtcx::PowerMode::NormalMode, &mut delay);
            println!("COLD SHTC3:      {:?}", shtc_meas);
        }

        #[cfg(feature = "heat_mat")]
        {
            let _ = ds18b20::start_simultaneous_temp_measurement(&mut one_wire_bus, &mut delay);
            Resolution::Bits12.delay_for_measurement_time(&mut delay);
            let search_state = None;
            let search = one_wire_bus.device_search(search_state.as_ref(), false, &mut delay);
            if let Some((device_address, _state)) = search.unwrap() {
                let fc = device_address.family_code();
                if fc != ds18b20::FAMILY_CODE {
                    println!("Other device: {:?}", fc);
                } else {
                    let sensor = Ds18b20::new::<&str>(device_address).unwrap();
                    let sensor_data = sensor.read_data(&mut one_wire_bus, &mut delay).unwrap();
                    println!(
                        "HEAT SENSOR:     Device at {:?} is {}°C",
                        device_address, sensor_data.temperature
                    );
                }
            }
        }

        #[cfg(any(feature = "warm_leds", feature = "cool_leds"))]
        {
            print!("LEDs:            ");
            for duty in (0..=100).step_by(10) {
                let duty = apply_gamma_correction(
                    scale_value(u16::MAX, duty as f32 / 100.0).unwrap(),
                    2.8,
                )
                .unwrap() as u32;
                print!("{}", duty);
                #[cfg(feature = "warm_leds")]
                {
                    let warm_result = led_warm.set_duty(duty);
                    print!("|w:{:?}", warm_result);
                }
                #[cfg(feature = "warm_leds")]
                {
                    let cool_result = led_cool.set_duty(duty);
                    print!("|c:{:?}", cool_result);
                }
                print!(" ");
                std::thread::sleep(std::time::Duration::from_millis(50));
            }
            println!();
        }
    }
}
